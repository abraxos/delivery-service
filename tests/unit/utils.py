from pathlib import Path
from typing import Optional, List
from subprocess import run

from typeguard import typechecked


@typechecked
def deep_touch(path: Path,
               content: Optional[str] = None,
               size: Optional[str] = None) -> None:
    if not path.exists():
        path.parent.mkdir(parents=True, exist_ok=True)
        path.touch()
    if content:
        path.open('w').write(content)
    elif size:
        run(['bash', '-c', f"head -c {size} /dev/urandom > {str(path)}"],
            check=True)


@typechecked
def contents(path: Path) -> str:
    return str(path.open().read())


@typechecked
def report(update: str) -> None:
    print(update)


UNITS: List[str] = [' bytes', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB']


@typechecked
def human_size(num_bytes: int, units=UNITS):
    return str(num_bytes) + units[0] if num_bytes < 1024 else \
               human_size(num_bytes >> 10, units[1:])


@typechecked
def file_size(file_path: Path):
    return human_size(file_path.stat().st_size)
