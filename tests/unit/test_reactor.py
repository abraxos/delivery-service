from pathlib import Path
from tempfile import TemporaryDirectory
from time import sleep
from subprocess import run

from typeguard import typechecked
from pytest import mark

from delivery_service.reactor import Reactor, is_child, open_for_writing


@typechecked
def test_is_child() -> None:
    path_a = Path.cwd()
    path_b = Path.cwd().parent
    assert is_child(path_a, path_b)
    assert not is_child(path_b, path_a)


@typechecked
def test_open_for_writing_file() -> None:
    with TemporaryDirectory() as dir_name:
        temp_dir = Path(dir_name)
        stuff = temp_dir / 'stuff.txt'
        with stuff.open('w+') as to_write:
            to_write.write('stuff')
            assert open_for_writing(stuff)
            assert open_for_writing(temp_dir)
            to_write.write('more stuff')
        assert not open_for_writing(stuff)
    assert not open_for_writing(stuff)


@typechecked
@mark.slow
def test_reactor_basics() -> None:
    with TemporaryDirectory() as dir_name:
        temp_dir = Path(dir_name)
        track = temp_dir / 'file_updates.txt'
        @typechecked
        def add_updated_file_to_list_file(src: Path) -> None:
            track.open('a+').write(str(src) + '\n')
        watch_dir = temp_dir / 'watch'
        watch_dir.mkdir(exist_ok=True)
        reactor = Reactor(watch_dir, add_updated_file_to_list_file)
        (watch_dir / 'a.txt').touch()
        (watch_dir / 'b.txt').touch()
        (watch_dir / 'c.txt').touch()
        (watch_dir / 'd.txt').touch()
        sleep(1.5)
        (temp_dir / 'e.txt').touch()
        (temp_dir / 'e.txt').rename(watch_dir / 'e.txt')
        sleep(1)
        modified = set(l.strip() for l in track.open('r').readlines())
        assert str(watch_dir / 'a.txt') in modified
        assert str(watch_dir / 'b.txt') in modified
        assert str(watch_dir / 'c.txt') in modified
        assert str(watch_dir / 'd.txt') in modified
        assert str(watch_dir / 'e.txt') in modified
        reactor.stop()


@typechecked
@mark.slow
def test_reactor_with_tracking() -> None:
    with TemporaryDirectory() as dir_name:
        temp_dir = Path(dir_name)
        track1 = temp_dir / 'file_updates1.txt'
        track2 = temp_dir / 'file_updates2.txt'
        track_dir = temp_dir / 'track'
        @typechecked
        def add_updated_file_to_first_list_file(src: Path) -> None:
            track1.open('a+').write(str(src) + '\n')

        @typechecked
        def add_updated_file_to_second_list_file(src: Path) -> None:
            track2.open('a+').write(str(src) + '\n')
        watch_dir = temp_dir / 'watch'
        watch_dir.mkdir()
        track_dir.mkdir()
        # first reactor run
        reactor = Reactor(watch_dir,
                          add_updated_file_to_first_list_file,
                          track_dir)
        (watch_dir / 'a.txt').touch()
        (watch_dir / 'b.txt').touch()
        (watch_dir / 'c.txt').touch()
        (watch_dir / 'd.txt').touch()
        sleep(1.5)
        (temp_dir / 'e.txt').touch()
        (temp_dir / 'e.txt').rename(watch_dir / 'e.txt')
        sleep(1)
        modified = set(l.strip() for l in track1.open('r').readlines())
        assert str(watch_dir / 'a.txt') in modified
        assert str(watch_dir / 'b.txt') in modified
        assert str(watch_dir / 'c.txt') in modified
        assert str(watch_dir / 'd.txt') in modified
        assert str(watch_dir / 'e.txt') in modified
        reactor.stop()
        # touch some files
        sleep(0.5)
        (watch_dir / 'f.txt').touch()
        (watch_dir / 'g.txt').touch()
        sleep(0.5)
        # second reactor run
        reactor = Reactor(watch_dir,
                          add_updated_file_to_second_list_file,
                          track_dir)
        (temp_dir / 'e.txt').touch()
        (temp_dir / 'e.txt').rename(watch_dir / 'e.txt')
        sleep(1)
        modified = set(l.strip() for l in track2.open('r').readlines())
        assert str(watch_dir / 'a.txt') not in modified
        assert str(watch_dir / 'b.txt') not in modified
        assert str(watch_dir / 'c.txt') not in modified
        assert str(watch_dir / 'd.txt') not in modified
        assert str(watch_dir / 'e.txt') in modified
        assert str(watch_dir / 'f.txt') in modified
        assert str(watch_dir / 'g.txt') in modified
        reactor.stop()


@typechecked
@mark.slow
def test_reactor_with_subprocess_callback() -> None:
    with TemporaryDirectory() as dir_name:
        temp_dir = Path(dir_name)
        track = temp_dir / 'file_updates.txt'
        @typechecked
        def add_updated_file_to_list_file(src: Path) -> None:
            track.open('a+').write(str(src) + '\n')
            run(['sleep', '5m'], check=True)
        watch_dir = temp_dir / 'watch'
        watch_dir.mkdir(exist_ok=True)
        reactor = Reactor(watch_dir, add_updated_file_to_list_file)
        (watch_dir / 'a.txt').touch()
        sleep(1)
        modified = set(l.strip() for l in track.open('r').readlines())
        assert str(watch_dir / 'a.txt') in modified
        reactor.stop()
