from time import sleep
from tempfile import TemporaryDirectory
from multiprocessing import Process
from pathlib import Path
from os import access, R_OK, W_OK, X_OK, chmod
from stat import S_IRWXU

from click.testing import CliRunner
from typeguard import typechecked
from pytest import mark

from delivery_service.cli import main, utils
from delivery_service.tracker import is_processed
from utils import deep_touch, file_size


@typechecked
def test_cli_utils_mark() -> None:
    with TemporaryDirectory() as dir_name:
        temp_dir = Path(dir_name)
        test_dir = temp_dir / 'test'
        track_dir = temp_dir / 'track'
        test_dir.mkdir()
        track_dir.mkdir()
        test1 = test_dir / 'test1.txt'
        test2 = test_dir / 'test2.txt'
        stuff1 = test_dir / 'stuff1.txt'
        moar1 = test_dir / 'moar1.txt'
        test1.touch()
        test2.touch()
        stuff1.touch()
        moar1.touch()
        res = CliRunner().invoke(utils, ['mark', '-t', str(track_dir),
                                         str(test_dir) + '/test*',
                                         str(test_dir) + '/stuff*'])
        assert res.exit_code == 0
        assert is_processed(track_dir, test1)
        assert is_processed(track_dir, test2)
        assert is_processed(track_dir, stuff1)
        assert not is_processed(track_dir, moar1)


@typechecked
@mark.slow
def test_cli_basic_sync() -> None:
    with TemporaryDirectory() as dir_name:
        path = Path(dir_name)
        c_src = path / 'c-src'
        m_src = path / 'm-src'
        dst = path / 'dst'
        c_src.mkdir()
        m_src.mkdir()
        dst.mkdir()

        def run_daemon() -> None:
            CliRunner().invoke(main, ['-c', str(c_src), str(dst),
                                      '-m', str(m_src), str(dst)])
        proc = Process(target=run_daemon)
        proc.start()
        sleep(1)
        deep_touch(c_src / 'copy', size='5MiB')
        deep_touch(m_src / 'move', size='6MiB')
        sleep(9)
        assert (dst / 'copy').exists()
        assert (dst / 'move').exists()
        assert not (m_src / 'move').exists()
        assert file_size(dst / 'copy') == '5MiB'
        assert file_size(dst / 'move') == '6MiB'
        proc.terminate()


@typechecked
@mark.slow
def test_cli_chmod_sync() -> None:
    with TemporaryDirectory() as dir_name:
        path = Path(dir_name)
        c_src = path / 'c-src'
        m_src = path / 'm-src'
        dst = path / 'dst'
        c_src.mkdir()
        m_src.mkdir()
        dst.mkdir()

        def run_daemon() -> None:
            CliRunner().invoke(main, ['-c', str(c_src), str(dst),
                                      '-m', str(m_src), str(dst),
                                      '--chmod', 'Du+w,Dugo+rx,Dgo-w,Fu+w,'
                                      'Fugo+r,Fgo-w,Fugo-x'])
        proc = Process(target=run_daemon)
        proc.start()
        sleep(1)
        deep_touch(c_src / 'copy', size='5MiB')
        chmod(c_src / 'copy', S_IRWXU)
        deep_touch(m_src / 'move', size='6MiB')
        chmod(m_src / 'move', S_IRWXU)
        sleep(4)
        assert (dst / 'copy').exists()
        assert (dst / 'move').exists()
        assert not (m_src / 'move').exists()
        assert file_size(dst / 'copy') == '5MiB'
        assert file_size(dst / 'move') == '6MiB'
        assert access(dst / 'copy', R_OK)
        assert access(dst / 'copy', W_OK)
        assert not access(dst / 'copy', X_OK)
        assert access(dst / 'move', R_OK)
        assert access(dst / 'move', W_OK)
        assert not access(dst / 'move', X_OK)
        proc.terminate()


@typechecked
def test_cli_error() -> None:
    with TemporaryDirectory() as dir_name:
        path = Path(dir_name)
        c_src = path / 'c-src'
        m_src = path / 'm-src'
        dst = path / 'dst'
        result = CliRunner().invoke(main, ['-c', str(c_src), str(dst),
                                           '-m', str(m_src), str(dst)])
        assert result.exit_code == 2
