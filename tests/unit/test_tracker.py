from tempfile import TemporaryDirectory
from pathlib import Path

from typeguard import typechecked

from delivery_service.tracker import FileSymlinkTracker, mark_processed
from delivery_service.tracker import mark_unprocessed, is_processed
from utils import deep_touch


@typechecked
def test_tracker_basics() -> None:
    with TemporaryDirectory() as dir_name:
        temp_dir = Path(dir_name)
        tracker_dir = temp_dir / 'tracker'
        tracker_dir.mkdir()
        tracked_dir = temp_dir / 'tracked'
        tracked_dir.mkdir()
        tracker = FileSymlinkTracker(tracker_dir)
        stuff1 = tracked_dir / 'stuff1'
        stuff2 = tracked_dir / 'stuff2'
        stuff0 = tracker_dir / 'stuff0'
        deep_touch(stuff1)
        deep_touch(stuff2)
        deep_touch(stuff0)
        assert not tracker.is_processed(stuff1)
        tracker.mark_processed(stuff1)
        tracker.mark_processed(stuff1)
        tracker.mark_processed(stuff2)
        assert tracker.is_processed(stuff1)
        assert tracker.is_processed(stuff2)
        tracker.mark_unprocessed(stuff1)
        assert not tracker.is_processed(stuff1)
        stuff2.unlink()
        assert not tracker.is_processed(stuff2)


@typechecked
def test_tracker_wrapper_functions() -> None:
    with TemporaryDirectory() as dir_name:
        temp_dir = Path(dir_name)
        tracker_dir = temp_dir / 'tracker'
        tracker_dir.mkdir()
        tracked_dir = temp_dir / 'tracked'
        tracked_dir.mkdir()
        stuff1 = tracked_dir / 'stuff1'
        stuff2 = tracked_dir / 'stuff2'
        stuff0 = tracker_dir / 'stuff0'
        deep_touch(stuff1)
        deep_touch(stuff2)
        deep_touch(stuff0)
        assert not is_processed(tracker_dir, stuff1)
        mark_processed(tracker_dir, stuff1)
        mark_processed(tracker_dir, stuff1)
        mark_processed(tracker_dir, stuff2)
        assert is_processed(tracker_dir, stuff1)
        assert is_processed(tracker_dir, stuff2)
        mark_unprocessed(tracker_dir, stuff1)
        assert not is_processed(tracker_dir, stuff1)
        stuff2.unlink()
        assert not is_processed(tracker_dir, stuff2)
