from tempfile import TemporaryDirectory
from pathlib import Path
from subprocess import CalledProcessError

from typeguard import typechecked
from pytest import raises, mark

from delivery_service.rsync import RSync, RSyncInsufficientlyConfigured
from delivery_service.rsync import RSyncNode, InvalidRSyncSourceOrDestination
from delivery_service.rsync import IncompatibleRSyncParameters
from utils import deep_touch, report, file_size


@typechecked
def test_rsync_node_creation_string() -> None:
    node = RSyncNode('something-something.txt')
    assert not node.is_remote


@typechecked
def test_rsync_node_creation_path() -> None:
    node = RSyncNode(Path('something-something.txt'))
    assert not node.is_remote


@typechecked
def test_rsync_remote_source() -> None:
    node = RSyncNode('a@b:22/c/d/e.txt')
    assert node.is_remote
    assert node.cmd == ['-e', 'ssh -p 22 -l a', 'b:/c/d/e.txt']


@typechecked
def test_rsync_node_creation_invalid_string() -> None:
    with raises(InvalidRSyncSourceOrDestination):
        node = RSyncNode('')
        assert node.is_remote


@typechecked
def test_validation() -> None:
    with TemporaryDirectory() as dir_name:
        path = Path(dir_name)
        deep_touch(path / 'something-something.txt', 'stuff')
        deep_touch(path / 'dst' / '1.txt')
        with raises(RSyncInsufficientlyConfigured):
            RSync().source(str(path / 'something-something.txt')).run()
        with raises(RSyncInsufficientlyConfigured):
            RSync().destination(str(path / 'dst')).run()
        assert not RSync().source('a@b:22/c/d/e.txt')\
                          .destination('dst')._extant_path('dst.txt')
        with raises(IncompatibleRSyncParameters):
            RSync().source('a@b:22/c/d/e.txt')\
                   .destination('a@b:22/c/d/e.txt')._validate()


@typechecked
@mark.slow
def test_rsync_basics() -> None:
    files_synced = set()
    dirs_synced = set()

    @typechecked
    def files(path: Path) -> None:
        if path.is_dir():
            dirs_synced.add(path)
        else:
            files_synced.add(path)

    @typechecked
    def progress(bts: str, complete: str, speed: str, eta: str) -> None:
        print(bts, complete, speed, eta)

    @typechecked
    def file_progress(path: Path, bts: str, complete: str, speed: str,
                      eta: str) -> None:
        print(path, bts, complete, speed, eta)

    with TemporaryDirectory() as dir_name:
        path = Path(dir_name)
        deep_touch(path / 'src' / 'a' / 'b' / 'c' / '1.txt', size='5MiB')
        deep_touch(path / 'src' / 'a' / 'b' / 'c' / '2.txt', size='5MiB')
        deep_touch(path / 'src' / 'a' / 'b' / 'c' / '3.txt', size='5MiB')
        deep_touch(path / 'src' / 'a' / 'b' / 'c' / '4.txt', size='5MiB')
        deep_touch(path / 'src' / 'a' / 'b' / 'c' / '5.txt', size='5MiB')
        deep_touch(path / 'src' / 'something-something.txt', size='5MiB')
        deep_touch(path / 'dst' / 'src' / 'a' / 'b' / 'c' / '1.txt')
        RSync().source(str(path / 'src'))\
               .destination(str(path / 'dst'))\
               .on_output(report)\
               .on_sync_file(files)\
               .on_update(progress)\
               .on_file_progress(file_progress)\
               .chmod('Du+w,Dugo+rx,Dgo-w,Fu+w,Fugo+r,Fgo-w,Fugo-x')\
               .run()
        assert (path / 'dst' / 'src' / 'a' / 'b' / 'c' / '1.txt').exists()
        assert file_size(path / 'dst' / 'src' / 'a' / 'b' / 'c' / '1.txt') == '5MiB'
        assert (path / 'dst' / 'src' / 'a' / 'b' / 'c' / '2.txt').exists()
        assert file_size(path / 'dst' / 'src' / 'a' / 'b' / 'c' / '2.txt') == '5MiB'
        assert (path / 'dst' / 'src' / 'a' / 'b' / 'c' / '3.txt').exists()
        assert file_size(path / 'dst' / 'src' / 'a' / 'b' / 'c' / '3.txt') == '5MiB'
        assert (path / 'dst' / 'src' / 'a' / 'b' / 'c' / '4.txt').exists()
        assert file_size(path / 'dst' / 'src' / 'a' / 'b' / 'c' / '4.txt') == '5MiB'
        assert (path / 'dst' / 'src' / 'a' / 'b' / 'c' / '5.txt').exists()
        assert file_size(path / 'dst' / 'src' / 'a' / 'b' / 'c' / '5.txt') == '5MiB'

        assert path / 'src' / 'a' / 'b' / 'c' / '1.txt' in files_synced
        assert path / 'src' / 'a' / 'b' / 'c' / '2.txt' in files_synced
        assert path / 'src' / 'a' / 'b' / 'c' / '3.txt' in files_synced
        assert path / 'src' / 'a' / 'b' / 'c' / '4.txt' in files_synced
        assert path / 'src' / 'a' / 'b' / 'c' / '5.txt' in files_synced
        assert path / 'src' / 'something-something.txt' in files_synced
        assert len(files_synced) == 6


@typechecked
def test_rsync_single_file() -> None:
    with TemporaryDirectory() as dir_name:
        path = Path(dir_name)
        deep_touch(path / 'something-something.txt', size='5MiB')
        deep_touch(path / 'dst' / '1.txt')
        RSync().source(str(path / 'something-something.txt'))\
               .destination(str(path / 'dst'))\
               .run()
        assert (path / 'dst' / 'something-something.txt').exists()


@typechecked
def test_rsync_single_file_and_remove() -> None:
    with TemporaryDirectory() as dir_name:
        path = Path(dir_name)
        deep_touch(path / 'something-something.txt', size='5MiB')
        deep_touch(path / 'dst' / '1.txt')
        RSync().source(str(path / 'something-something.txt'))\
               .destination(str(path / 'dst'))\
               .remove_src()\
               .run()
        assert (path / 'dst' / 'something-something.txt').exists()
        assert not (path / 'something-something.txt').exists()


@typechecked
def test_rsync_exception_handling() -> None:
    with raises(CalledProcessError):
        with TemporaryDirectory() as dir_name:
            path = Path(dir_name)
            RSync().source(str(path / 'something-something.txt'))\
                   .destination(str(path / 'dst'))\
                   .remove_src()\
                   .run()
