from tempfile import TemporaryDirectory
from pathlib import Path
from time import sleep

from pytest import mark
from typeguard import typechecked

from delivery_service.reactor import Reactor
from delivery_service.rsync import RSync
from utils import deep_touch, file_size


@typechecked
@mark.slow
def test_rsync_with_reactor_basic() -> None:
    with TemporaryDirectory() as dir_name:
        temp_dir = Path(dir_name)
        watch_dir = temp_dir / 'watch_dir'
        dst_dir = temp_dir / 'dst'
        watch_dir.mkdir(exist_ok=True)
        dst_dir.mkdir(exist_ok=True)
        @typechecked
        def rsync_to_dst(src: Path) -> None:
            RSync().source(src).destination(dst_dir).run()
        sleep(0.5)
        reactor = Reactor(watch_dir, rsync_to_dst)
        sleep(3)
        deep_touch(watch_dir / 'a.txt', size='5MiB')
        sleep(6)
        while any(p.is_alive() for p in reactor.procs.values()):
            sleep(0.2)
        assert (dst_dir / 'a.txt').exists()
        assert file_size(dst_dir / 'a.txt') == '5MiB'
        reactor.stop()


@typechecked
@mark.slow
def test_rsync_large_file() -> None:
    with TemporaryDirectory() as dir_name:
        temp_dir = Path(dir_name)
        watch_dir = temp_dir / 'watch_dir'
        dst_dir = temp_dir / 'dst'
        watch_dir.mkdir(exist_ok=True)
        dst_dir.mkdir(exist_ok=True)
        @typechecked
        def rsync_to_dst(src: Path) -> None:
            RSync().source(src).destination(dst_dir).run()
        reactor = Reactor(watch_dir, rsync_to_dst)
        deep_touch(watch_dir / 'a.txt', size='500MiB')
        sleep(6)
        while any(p.is_alive() for p in reactor.procs.values()):
            sleep(0.2)
        assert (dst_dir / 'a.txt').exists()
        assert file_size(dst_dir / 'a.txt') == '500MiB'
        reactor.stop()


@typechecked
@mark.slow
def test_rsync_directory_with_large_files() -> None:
    with TemporaryDirectory() as dir_name:
        temp_dir = Path(dir_name)
        watch_dir = temp_dir / 'watch_dir'
        src_dir = temp_dir / 'src'
        dst_dir = temp_dir / 'dst'
        watch_dir.mkdir(exist_ok=True)
        src_dir.mkdir(exist_ok=True)
        dst_dir.mkdir(exist_ok=True)
        @typechecked
        def rsync_to_dst(src: Path) -> None:
            RSync().source(src).destination(dst_dir).run()
        reactor = Reactor(watch_dir, rsync_to_dst)
        deep_touch(src_dir / 'a.txt', size='500MiB')
        deep_touch(src_dir / 'b.txt', size='250MiB')
        deep_touch(src_dir / 'c.txt', size='100MiB')
        src_dir.rename(dst_dir / 'src')
        sleep(0.5)
        while any(p.is_alive() for p in reactor.procs.values()):
            sleep(0.2)
        assert (dst_dir / 'src' / 'a.txt').exists()
        assert (dst_dir / 'src' / 'b.txt').exists()
        assert (dst_dir / 'src' / 'c.txt').exists()
        assert file_size(dst_dir / 'src' / 'a.txt') == '500MiB'
        assert file_size(dst_dir / 'src' / 'b.txt') == '250MiB'
        assert file_size(dst_dir / 'src' / 'c.txt') == '100MiB'
        reactor.stop()
