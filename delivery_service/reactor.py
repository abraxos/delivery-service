from typing import Callable, Dict, Optional, Union
from pathlib import Path
from multiprocessing import Process
from time import sleep

from psutil import process_iter, AccessDenied, NoSuchProcess
from typeguard import typechecked
from watchdog.events import FileSystemEvent, FileSystemEventHandler
from watchdog.observers import Observer

from delivery_service.tracker import mark_processed, is_processed
from delivery_service.log import log


@typechecked
def is_child(child: Path, parent: Path) -> bool:
    assert child.exists() and parent.is_dir()
    return any(par for par in child.parents if par == parent)


@typechecked
def open_for_writing(file_path: Path) -> bool:
    file_path.resolve()
    for proc in process_iter():
        try:
            if file_path.is_file() and any(pf for pf in proc.open_files() if
                                           ('w' in pf.mode or '+' in pf.mode)
                                           and pf.path == str(file_path)):
                return True
            if file_path.is_dir() and any(pf for pf in proc.open_files() if
                                          ('w' in pf.mode or '+' in pf.mode)
                                          and is_child(Path(pf.path),
                                                       file_path)):
                return True
        except (NoSuchProcess, AccessDenied, PermissionError):
            pass
    return False


class Reactor():
    class Handler(FileSystemEventHandler):
        @typechecked
        def __init__(self, watch_dir: Path,
                     callback: Callable[[Path], None],
                     track_dir: Union[Path, str, None] = None):
            self.watch_dir = watch_dir
            self.procs: Dict[Path, Process] = {}
            self.callback = callback
            self.track_dir: Optional[Path] = Path(track_dir) if track_dir is not None else None

        @staticmethod
        def _wait_until_file_write_complete(file_path: Path) -> None:
            while open_for_writing(file_path):
                sleep(0.5)

        @staticmethod
        def wrapped_callback(event_path: Path,
                             callback: Callable[[Path], None],
                             track_dir: Optional[Path]) -> None:
            try:
                callback(event_path)
                if track_dir:
                    mark_processed(track_dir, event_path)
            except Exception:
                log.warning(f'Unable to process file: {event_path}')

        @typechecked
        def process_if_new(self, file_path: Path) -> None:
            if self.track_dir is None or not is_processed(self.track_dir,
                                                          file_path):
                sleep(2)  # prevents too many ssh login attempts at once
                self._process_file(file_path)

        @typechecked
        def _process_file(self, file_path: Path) -> None:
            proc = Process(target=self.wrapped_callback,
                           args=(file_path,
                                 self.callback,
                                 self.track_dir))
            self.procs[file_path] = proc
            proc.start()

        @typechecked
        def react(self, src: Path):
            for path, proc in list(self.procs.items()):
                if not proc.is_alive():
                    del self.procs[path]
            if src not in self.procs and src != self.watch_dir:
                self._process_file(src)

        @typechecked
        def on_modified(self, event: FileSystemEvent) -> None:
            self._wait_until_file_write_complete(Path(event.src_path))
            self.react(Path(event.src_path))

        @typechecked
        def on_created(self, event: FileSystemEvent) -> None:
            self.on_modified(event)

        @typechecked
        def stop(self) -> None:
            for proc in self.procs.values():
                proc.terminate()
                proc.join()

    @typechecked
    def __init__(self, watch_dir: Path,
                 callback: Callable[[Path], None],
                 track_dir: Union[Path, str, None] = None):
        self.observer = Observer()
        self.handler = Reactor.Handler(watch_dir, callback, track_dir)
        self.observer.schedule(self.handler, str(watch_dir))
        self.observer.start()
        if track_dir is not None:
            # if we're tracking, process all unprocessed files
            for child in watch_dir.iterdir():
                self.handler.process_if_new(child)

    @typechecked
    def stop(self) -> None:
        self.observer.stop()
        self.join()
        self.handler.stop()

    @typechecked
    def join(self) -> None:
        self.observer.join()

    @property
    def procs(self) -> Dict[Path, Process]:
        return self.handler.procs
