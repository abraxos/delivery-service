from pathlib import Path
from typing import Generator, Optional
from uuid import uuid4 as UUID

from typeguard import typechecked


class FileSymlinkTracker():
    '''
    An object designed to track files that have somehow been processed by
    storing symlinks to those files and periodically cleaning them up.
    Specifically, when a file is processed, a symlink to it is created in a
    given directory. We can then check whether a file has been processed
    by seeing if there is a symlink pointing to it. This approach is not fast,
    but generally acceptable for I/O purposes and extremely simple to use,
    understand, and interfere with.
    '''
    @typechecked
    def __init__(self, db_dir: Path):
        assert db_dir.is_dir(), 'The file-tracking directory must exist'
        self.db_dir = db_dir.resolve()

    @typechecked
    def _symlinks(self) -> Generator[Path, None, None]:
        for child in self.db_dir.iterdir():
            if child.is_symlink():
                yield child

    @typechecked
    def link_for(self, file_path: Path) -> Optional[Path]:
        for link in self._symlinks():
            if link.resolve() == file_path.resolve():
                return link
        return None

    @typechecked
    def links_for(self, file_path: Path) -> Generator[Path, None, None]:
        for link in self._symlinks():
            if link.resolve() == file_path.resolve():
                yield link

    @typechecked
    def cleanup(self) -> None:
        '''
        Meant to be run before any check, this function scans the db
        directory and deletes any symlinks that point to non-existent files
        '''
        for link in self._symlinks():
            if not link.exists():
                link.unlink()

    @typechecked
    def mark_processed(self, file_path: Path) -> Path:
        self.cleanup()
        link = self.link_for(file_path)
        if not link:
            link = self.db_dir / str(UUID())
            link.symlink_to(file_path.resolve())
        return link

    @typechecked
    def mark_unprocessed(self, file_path) -> None:
        self.cleanup()
        for link in self.links_for(file_path):
            link.unlink()

    @typechecked
    def is_processed(self, file_path: Path) -> bool:
        self.cleanup()
        return self.link_for(file_path) is not None


@typechecked
def mark_processed(db_dir: Path, file_path: Path) -> Path:
    return FileSymlinkTracker(db_dir).mark_processed(file_path)


@typechecked
def mark_unprocessed(db_dir: Path, file_path: Path) -> None:
    return FileSymlinkTracker(db_dir).mark_unprocessed(file_path)


@typechecked
def is_processed(db_dir: Path, file_path: Path) -> bool:
    return FileSymlinkTracker(db_dir).is_processed(file_path)
