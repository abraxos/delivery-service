from logging import DEBUG
from collections import defaultdict
from logging import DEBUG, INFO

from logzero import loglevel as set_loglevel
from logzero import logger as log, LogFormatter, formatter as set_formatter

DEFAULT_FMT = '%(color)s[%(levelname)1.1s %(asctime)s]%(end_color)s %(message)s'
DEBUG_FMT = '%(color)s[%(levelname)1.1s %(asctime)s PID:%(process)d(%(processName)s)<%(threadName)s>%(module)s:%(funcName)s:%(lineno)d]%(end_color)s %(message)s'


class DefaultLogFormatter(LogFormatter):
    def __init__(self, formatters,
                 default_format=DEFAULT_FMT,
                 date_format='%Y-%m-%d %H:%M:%S%z'):
        self.formats = defaultdict(lambda: default_format)
        for key, value in formatters.items():
            self.formats[key] = value
        LogFormatter.__init__(self, fmt=default_format, datefmt=date_format)

    def format(self, record):
        self._fmt = self.formats[record.levelno]
        return LogFormatter.format(self, record)


set_formatter(DefaultLogFormatter({DEBUG: DEBUG_FMT},
                                  default_format=DEFAULT_FMT))
