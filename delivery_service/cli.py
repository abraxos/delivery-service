from pathlib import Path
from typing import NamedTuple, Optional, Any, Iterable
from functools import partial
from signal import signal, SIGTERM
from glob import iglob

import click
from typeguard import typechecked

from delivery_service.rsync import RSync, RSyncNode
from delivery_service.tracker import mark_processed
from delivery_service.reactor import Reactor
from delivery_service.log import log, DEBUG, INFO, set_loglevel

READABLE_DIR = click.Path(exists=True, file_okay=False, dir_okay=True,
                          writable=False, readable=True, resolve_path=True,
                          allow_dash=False)
WRITABLE_DIR = click.Path(exists=True, file_okay=False, dir_okay=True,
                          writable=True, readable=True, resolve_path=True,
                          allow_dash=False)


class Mapping(NamedTuple):
    src: Path
    dst: RSyncNode
    delete: bool = False


@typechecked
def log_file_progress(path: Path, _: str, complete: str,
                      speed: str, eta: str,
                      mapping: Mapping) -> None:
    log.info(f'Transferring: '
             f'{str(path.relative_to(Path(mapping.src)))} - '
             f'{complete} Complete, {speed} ETA: {eta}')


@typechecked
def rsync(path: Path, delivery: Mapping, chmod: Optional[str]) -> None:
    job = RSync().source(path).destination(delivery.dst)
    if chmod:
        job.chmod(chmod)
    job.on_file_progress(partial(log_file_progress, mapping=delivery))
    log.info(f"Starting transfer: {str(path)} ~> {delivery.dst}")
    job.run()
    log.info(f"Transfer of {str(path)} is complete, verifying...")
    if delivery.delete:
        job.remove_src()
    job.run()
    log.info(f"Transfer of {str(path)} is complete.")


@click.command()
@click.option('-c', '--copy-dir-map', type=(READABLE_DIR, str), multiple=True,
              default=None,
              help='A directory to monitor as well as a valid RSYNC '
                   'destination for files to be sent/updated to. Can be '
                   'remote or local. This option can be used multiple times '
                   'to monitor multiple directories. Files are copied/updated '
                   'to the destination and not deleted at the source')
@click.option('-m', '--move-dir-map', type=(READABLE_DIR, str), multiple=True,
              default=None,
              help='A directory to monitor as well as a valid RSYNC '
                   'destination for files to be sent/updated to. Can be '
                   'remote or local. This option can be used multiple times '
                   'to monitor multiple directories. Files are moved to the '
                   'destination and deleted at the source.')
@click.option('--chmod', type=str, help='Chmod string to be used with RSync to'
                                        'enforce permissions for transferred '
                                        'files.')
@click.option('-t', '--track-dir', type=WRITABLE_DIR,
              default=None,
              help='A directory that can optionally be used to keep track of '
                   'which files have already been processed in-between '
                   'launches of delivery-service. Any files that have been '
                   'processed successfully will have a symlink to them placed '
                   'in this directory. If a symlink exists, the file will not '
                   'be processed again when delivery-service is started '
                   'again. Any symlinks in this directory pointing to deleted '
                   'files will themselves be deleted.')
@click.option('--debug', is_flag=True, help='Set logging level to DEBUG')
@typechecked
def main(copy_dir_map: Any,
         move_dir_map: Any,
         chmod: Optional[str],
         track_dir: Optional[str],
         debug: bool) -> None:
    """An inotify-based system for automatically transferring files from
       watched directories using rsync"""
    set_loglevel(DEBUG if debug else INFO)
    copy_dir_map = copy_dir_map if copy_dir_map is not None else []
    move_dir_map = move_dir_map if move_dir_map is not None else []
    copy_mappings = [Mapping(src=Path(m[0]),
                             dst=RSyncNode(m[1]),
                             delete=False) for m in copy_dir_map]
    move_mappings = [Mapping(src=Path(m[0]),
                             dst=RSyncNode(m[1]),
                             delete=True) for m in move_dir_map]
    reactors = [Reactor(m.src,
                        partial(rsync,
                                chmod=chmod,
                                delivery=m),
                        track_dir) for m in copy_mappings] + \
               [Reactor(m.src,
                        partial(rsync,
                                chmod=chmod,
                                delivery=m),
                        track_dir) for m in move_mappings]

    @typechecked
    def _cleanup() -> None:
        for reactor in reactors:
            reactor.stop()
    signal(SIGTERM, _cleanup)

    for reactor in reactors:
        reactor.join()


@click.group()
@typechecked
def utils() -> None:
    '''A set of useful utilities for delivery-service'''


@utils.command()
@click.option('-t', '--track-dir', type=WRITABLE_DIR,
              required=True,
              help='A writeable directory that delivery-service can use to '
                   'track processed files via symlinks. See `delivery-service '
                   '--help` for more details')
@click.argument('file_patterns', nargs=-1)
@typechecked
def mark(track_dir: str, file_patterns: Iterable[str]) -> None:
    '''
    Given a tracking directory, this script will mark all given files as
    processed for the purposes of the delivery-service program. This script
    is intended to mark a bunch of files as processed without delivery-service
    actually processing them in the event where it should not for whatever
    reason.
    '''
    for pattern in file_patterns:
        for file_path in iglob(pattern):
            print(f'Marking {file_path} processed')
            mark_processed(Path(track_dir), Path(file_path))
