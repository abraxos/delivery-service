from typing import Optional, List, Generator, Callable, Union
from pathlib import Path
from re import compile as regex
from subprocess import Popen, PIPE
from subprocess import CalledProcessError

from typeguard import typechecked

from delivery_service.log import log

FAIL_LIMIT = 3
PARTIAL_LIMIT = 20


class InvalidRSyncSourceOrDestination(Exception):
    pass


class RSyncInsufficientlyConfigured(Exception):
    pass


class IncompatibleRSyncParameters(Exception):
    pass


class RSyncNode():
    SRC_DST_PAT = regex(r'^(((?P<user>\S+)@)?(?P<server>(\S+)):)?'
                        r'(?P<port>\d*)?(?P<path>/?.+)$')

    @typechecked
    def __init__(self, node: Union[str, Path]) -> None:
        self.user = None
        self.port = None
        self.srv = None
        if isinstance(node, str):
            match = RSyncNode.SRC_DST_PAT.match(node)
            if match:
                self.user = match['user'] if match['user'] else None
                self.path = match['path'] if match['path'] else None
                self.port = int(match['port']) if match['port'] else None
                self.srv = match['server'] if match['server'] else None
                return
            raise InvalidRSyncSourceOrDestination(f'{node} is not a valid '
                                                  'RSync source/destination')
        assert isinstance(node, Path)
        self.path = str(node)

    @property
    def is_remote(self) -> bool:
        return self.srv is not None

    @property
    def cmd(self) -> List[str]:
        if self.srv:
            return ['-e', 'ssh' +
                    (f' -p {self.port}' if self.port else '') +
                    (f' -l {self.user}' if self.user else ''),
                    f'{self.srv}:{self.path}']
        return [self.path]

    @typechecked
    def __repr__(self) -> str:
        if self.is_remote:
            return f'{self.user}@{self.srv}:{self.port}{self.path}'
        return str(self.path)


Progress = Callable[[str, str, str, str], None]
FileProgress = Callable[[Path, str, str, str, str], None]


class RSync():
    UPDATE_PAT = regex(r'\s*(?P<bytes>[\d,]+)\s+(?P<complete>\d+%)\s+'
                       r'(?P<speed>[\d.]+.B/s)\s+(?P<eta>\d\d?:\d\d:\d\d)')

    @typechecked
    def __init__(self) -> None:
        self._proc: Optional[Popen] = None
        self.src: Optional[RSyncNode] = None
        self.dst: Optional[RSyncNode] = None
        self._chmod: Optional[str] = None
        self._remove_src = False

        self.output_callback: Optional[Callable[[str], None]] = None
        self.update_callback: Optional[Progress] = None
        self.file_progress_callback: Optional[FileProgress] = None
        self.file_callback: Optional[Callable[[Path], None]] = None

    @typechecked
    def source(self, src: Union[str, Path]) -> 'RSync':
        self.src = RSyncNode(src) if not isinstance(src, RSyncNode) else src
        return self

    @typechecked
    def destination(self, dst: Union[str, Path, RSyncNode]) -> 'RSync':
        self.dst = RSyncNode(dst) if not isinstance(dst, RSyncNode) else dst
        return self

    @typechecked
    def chmod(self, chmod: str) -> 'RSync':
        self._chmod = chmod
        return self

    @typechecked
    def remove_src(self, delete: bool = True) -> 'RSync':
        self._remove_src = delete
        return self

    @typechecked
    def _validate(self) -> None:
        if not self.src:
            raise RSyncInsufficientlyConfigured('No source configured')
        if not self.dst:
            raise RSyncInsufficientlyConfigured('No destination configured')
        if self.src.is_remote and self.dst.is_remote:
            raise IncompatibleRSyncParameters('Cannot have both source and '
                                              'destination servers')

    @typechecked
    def _rsync(self) -> Generator[str, None, None]:
        self._validate()
        if self.src and self.dst:
            params = ['-Ptrvy']
            if self._remove_src:
                params.append('--remove-source-files')
            chmod = [f'--chmod={self._chmod}'] if self._chmod else []
            cmd = ['rsync'] + params + chmod + self.src.cmd + self.dst.cmd
            log.debug(cmd)
            rsync_process = Popen(cmd, stdout=PIPE, universal_newlines=True)
            while 42:
                stdout_line = ''
                try:
                    stdout_line = rsync_process.stdout.readline()
                    if stdout_line == '':
                        break
                except UnicodeDecodeError:
                    pass
                yield stdout_line.strip()
            rsync_process.stdout.close()
            return_code = rsync_process.wait()
            if return_code:
                raise CalledProcessError(return_code, cmd)
        else:
            RSyncInsufficientlyConfigured('Invalid configuration')

    @typechecked
    def on_output(self, callback: Callable[[str], None]) -> 'RSync':
        self.output_callback = callback
        return self

    @typechecked
    def on_update(self, callback: Progress) -> 'RSync':
        self.update_callback = callback
        return self

    @typechecked
    def on_file_progress(self, callback: FileProgress) -> 'RSync':
        self.file_progress_callback = callback
        return self

    @typechecked
    def on_sync_file(self, callback: Callable[[Path], None]) -> 'RSync':
        self.file_callback = callback
        return self

    @typechecked
    def _extant_path(self, candidate: str) -> Optional[Path]:
        self._validate()
        if self.src and not self.src.is_remote and \
                (Path(self.src.path).parent / candidate).exists():
            return Path(self.src.path).parent / candidate
        if self.dst and not self.dst.is_remote and \
                (Path(self.dst.path).parent / candidate).exists():
            return Path(self.dst.path).parent / candidate
        return None

    @typechecked
    def _run(self) -> None:
        cur: Path = Path(self.src.path) if self.src else Path()
        for update in self._rsync():
            if update:
                update_match = RSync.UPDATE_PAT.match(update)
                if self.src and self._extant_path(update):
                    cur = Path(self.src.path).parent / update
                if update_match and self.update_callback:
                    self.update_callback(update_match['bytes'],
                                         update_match['complete'],
                                         update_match['speed'],
                                         update_match['eta'],)
                if update_match and self.file_progress_callback:
                    self.file_progress_callback(cur,
                                                update_match['bytes'],
                                                update_match['complete'],
                                                update_match['speed'],
                                                update_match['eta'],)
                if self.file_callback and self.src and \
                        self._extant_path(update):
                    self.file_callback(Path(self.src.path).parent / update)
                if self.output_callback is not None:
                    self.output_callback(update)

    @typechecked
    def run(self) -> None:
        self._validate()
        exception: Optional[CalledProcessError] = None
        fail_count = 0
        partial_count = 0
        while fail_count < FAIL_LIMIT and partial_count < PARTIAL_LIMIT:
            try:
                self._run()
                return
            except CalledProcessError as exc:
                log.warning(f'RSync Error Detected: {exc}')
                exception = exc
                if exc.returncode == 23:
                    partial_count += 1
                    log.warning(f'Partial transfer detected,{" not " if partial_count >= PARTIAL_LIMIT else " "}retrying')
                else:
                    fail_count += 1
                    log.error(f'RSync error detected,{" not " if fail_count >= FAIL_LIMIT else " "}retrying')
        assert exception is not None
        raise exception
