"""Setup file for delivery-service"""
from setuptools import setup, find_packages


DEV_REQ = ['pytest', 'pytest-cov', 'pylint', 'mypy', 'flake8']
REQ = ['click', 'pydantic', 'typeguard', 'watchdog', 'psutil', 'logzero']

with open('README.md') as f:
    README = f.read()

with open('LICENSE') as f:
    LICENSE = f.read()

setup(
    name='delivery-service',
    version='0.5.0',
    description='A service for automatically transferring files to remote servers as they are moved to watched local directories.',
    long_description=README,
    author='Eugene Kovalev',
    author_email='eugene@kovalev.systems',
    url='https://gitlab.com/abraxos/delivery-service',
    license=LICENSE,
    packages=find_packages(exclude=('tests', 'docs')),
    entry_points={'console_scripts':
                  ['delivery-service=delivery_service.cli:main',
                   'delivery-service-utils=delivery_service.cli:utils']},
    install_requires=REQ,
    extras_require={'dev': DEV_REQ}
)
