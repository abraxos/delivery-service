# Delivery Service

A service for automatically transferring files to remote servers as they are moved to watched local directories.

## Installation & Setup

To install delivery-service with [`pip`](https://pip.pypa.io/en/stable/) execute the following:

```bash
pip install /path/to/repo/delivery-service
```

If you don't want to re-install every time there is an update, and prefer to just pull from the git repository, then use the `-e` flag.

## Usage

For the most up-to-date instructions and details on how to use `delivery-service` simply use `delivery-service --help`.

## Development Setup

Using [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/) create a virtualenv:

```bash
mkvirtualenv --python=`which python3` delivery-service
(delivery-service)$ _
```

This will also activate the virtualenv as denoted by the shell prompt prefix: `(delivery-service)`. _Please note that all commands demonstrated here that are prefixed with `(delivery-service)$` are meant to be run from inside the virtualenv._ You can always exit the virtualenv with `deactivate` and get back to it with `workon delivery-service`.

Then install the python package (in-place) and its relevant pre-requisites for development with:

```bash
(delivery-service)$ pip install -e '.[dev]' --use-feature=2020-resolver
```

### Development

### Testing

All testing should be done with `pytest` which is installed with the `dev` requirements.

To run all the unit tests, execute the following from the repo directory:

```bash
(delivery-service)$ pytest
```

This should produce a coverage report in `/path/to/dewey-api/htmlcov/`

To run a specific test file:

```bash
(delivery-service)$ pytest tests/unit/test_reactor.py
```

To run a specific test:

```bash
(delivery-service)$ pytest tests/unit/test_reactor.py::test_reactor_basics
```
## Running Tests

You can run all the tests with pytest and coverage (or all the unit tests, by changing the path to `tests/unit`) like so inside the virtualenv (`pipenv shell`):

```bash
(delivery-service)$ pytest --runslow
```

_Note that you can skip the `--runslow` option in order to only do the quick tests, and remove `-s -vv` to simplify the output._
